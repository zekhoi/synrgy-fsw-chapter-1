# Binar Challenge Chapter 1

Khoironi Kurnia Syah - Fullstack Web Development 1 (A) - Chapter 1

Make a Landing Page with Bootstrap

- [x] Navigation Bar
- [x] Show Image
- [x] Carousel
- [x] Accordion
- [x] Footer

- Gitlab : [Gitlab](https://gitlab.com/zekhoi/synrgy-fsw-chapter-1)
- Github : [Github](https://github.com/synrgy-5-fsw-1-cihuy/synrgy-roni-fsw1-cihuy)
- Preview : [Open](https://zekhoi.gitlab.io/synrgy-fsw-chapter-1)
